# Deployken :sparkling_heart: Gitlab Example With Flutter

So, this is an example of Flutter app, that hosted in gitlab repository and intergrated with Deployken service

## Tutorial

- First, ensure you have to make same `flutter` project and dont forget init it as git project
- Push your existing git project to `gitlab`
- And next, adding deployment script to do it, just add `gitlab-ci.yml` into the root or project structure
- And you can follow `gitlab-ci.yml` in this repository
- Commit it, push it, and see the log
- Alhamdulillah, your beta-apps has been deployed!!!